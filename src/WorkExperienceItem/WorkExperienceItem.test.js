import { render, screen } from '@testing-library/react';
import WorkExperienceItem from './WorkExperienceItem';

test('renders learn react link', () => {
  render(<WorkExperienceItem />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

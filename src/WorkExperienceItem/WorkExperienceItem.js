import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap-grid.css";
import "./WorkExperienceItem.css";

export default class WorkExperienceItem extends React.Component {
  render() {
    return (
      <div class="container">
        <div class="row align-items-start">
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
        </div>
        <div class="row align-items-center">
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
        </div>
        <div class="row align-items-end">
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
          <div class="col">One of three columns</div>
        </div>
      </div>
    );
  }
}

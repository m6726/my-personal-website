import React from 'react';
import './App.css';
import WorkExperienceItem from './WorkExperienceItem/WorkExperienceItem';

export default class App extends React.Component {
  render() {
    return (
      <div className="App">
        <WorkExperienceItem></WorkExperienceItem>
      </div>
    );
  }
}